function assertPoint(assert, point1, point2, msg ){
	msg =  typeof msg !== 'undefined' ? msg : "Point has the expected coordinates";
	assert.ok(point1.x == point2.x && point2.y == point2.y, msg);
}

module('Point');

QUnit.test( "Point Construct", function( assert ) {
	var point = Point2D(2,3);
	assert.equal(point.x, 2, 'Point has the expected x coordinate');
 	assert.equal(point.y, 3, 'Point has the expected y coordinate');
});

QUnit.test( "Point Sum", function( assert ) {
	var point = Point2D(2,3);
	var pointResult = point.sum(Point2D(-2,4));
	assertPoint(assert,pointResult, Point2D(0,7));
});

QUnit.test( "Point Mul", function( assert ) {
	var point = Point2D(2,-3);
	var pointResult = point.mul(2);
 	assertPoint(assert,pointResult, Point2D(4,-6));
});

module('Circle');

QUnit.test( "Circle Constrct", function( assert ) {
	var circle = Circle(Point2D(2,2),Point2D(1,1),'black',20,0.5);
	assertPoint(assert,circle.coords,Point2D(2,2), "Circle coordinates are the expected");
	assertPoint(assert,circle.vel,Point2D(1,1), "Circle velocity is the expected");
	assert.equal(circle.color,'black','Circle color is the expected');
	assert.equal(circle.radius,20,'Circle radius is the expected');
	assert.equal(circle.elasticity,0.5,'Circle elasticity is the expected');

});

QUnit.test( "Circle Update Position", function( assert ) {
	var circle = Circle(Point2D(2,2),Point2D(1,1),'black',20,0.5);
	circle.update(Point2D(0,0),1);
	assertPoint(assert,circle.coords,Point2D(3,3), "Circle coordinates are the expected");
});

QUnit.test( "Circle Update Velocity", function( assert ) {
  	var circle = Circle(Point2D(0,0),Point2D(1,1),'black',20,0.5);
	circle.update(Point2D(2,2),1);
	assertPoint(assert,circle.vel,Point2D(3,3), "Circle velocity is the expected");
});


QUnit.test( "Circle Handle Colision", function( assert ) {
    var circle = Circle(Point2D(0,0),Point2D(100,0),'black',20,0.5);
    circle.update(Point2D(0,0),1);
    circle.handleColisions(Point2D(0,0),Point2D(50,50));
    assertPoint(assert,circle.coords,Point2D(50-circle.radius,0), "Circle coordinates are the expected");
    assertPoint(assert,circle.vel,Point2D(-50,0), "Circle velocity is the expected");
});

QUnit.test( "Circle Draw", function( assert ) {
    var circle = Circle(Point2D(25,25),Point2D(0,0),'red',20,0.5);
    canvasArea.width = window.innerWidth;
    canvasArea.height = window.innerHeight;
    context = canvasArea.getContext('2d');
    context.clearRect(0,0,window.innerWidth, window.innerHeight);
    circle.draw(context);
    var color = context.getImageData(25, 25, 1, 1).data;
    assert.ok(color[0] == 255 && color[1] == 0 && color[2] == 0, 'Center of the circle has the expected color');
    color = context.getImageData(100, 100, 1, 1).data;
    assert.ok(color[0] == 0 && color[1] == 0 && color[2] == 0, 'Ouside area has the expected color');

});

module('World');
QUnit.test( "World Construct", function( assert ) {
  var world = World(Point2D(1,1));
  assertPoint(assert,world.acel,Point2D(1,1), "Aceleration is the expected");
  world.setExtremePoints(Point2D(0,0),Point2D(50,50));
  assertPoint(assert,world.topLeftPoint,Point2D(0,0), "topLeftPoint is the expected");
  assertPoint(assert,world.bottonRightPoint,Point2D(50,50), "bottonRightPoint is the expected");
});

QUnit.test( "World Add Object", function( assert ) {
  var world = World(Point2D(1,1));
  world.addObj({});
  assert.equal(world.objs.length,1,"Exist one object in the world")
});

QUnit.test( "World Update", function( assert ) {
  	var calledHandleColision = false;
	var calledObjectUpdate = false;
	var world = World(Point2D(1,1));
   	world.addObj({
   	 	update: function(){
  			calledObjectUpdate = true;
  		},
  		handleColisions: function(){
  			calledHandleColision = true;
  		}
   });
   	world.update(1);
   assert.ok(!calledHandleColision, 'Handle colision was not called');
   assert.ok(calledObjectUpdate, 'Called object update');
   calledHandleColision= false;
   calledObjectUpdate = false;
   world.setExtremePoints(Point2D(0,0),Point2D(50,50));
   world.update(1);
   assert.ok(calledHandleColision, 'Handle colision was called');
   assert.ok(calledObjectUpdate, 'Called object update');
});

QUnit.test( "World Draw", function( assert ) {
	var calledObjectDraw = false;
	var cleanedDrawAred = false;
	  var world = World(Point2D(1,1));
  world.addObj({draw: function(){
  	calledObjectDraw = true;
  }});
  world.draw({clearRect: function(){
  	cleanedDrawAred = true;
  }});
  assert.ok(cleanedDrawAred, 'Called function to clean draw area');
  assert.ok(calledObjectDraw, 'Called function to draw object');
});