var DEFAULT_GRAVITY = 980;
var MAX_RANDOM_VELOCITY = 4000;
var CIRCLE_ELASTICITY = 0.5;
var CIRCLE_RADIUS = 20;
var CIRCLE_COLOR = 'black';
var UPDATE_SPEED_MILISECONDS = 10;

function main()
{
  var world = World(Point2D(0,DEFAULT_GRAVITY));
  context = canvasArea.getContext('2d');
  
  window.onresize = function(){
    canvasArea.width = window.innerWidth;
    canvasArea.height = window.innerHeight;
    world.setExtremePoints(Point2D(0,0),Point2D(window.innerWidth,window.innerHeight));
  }
  window.onresize();

  setInterval(function(){
    world.update(UPDATE_SPEED_MILISECONDS/1000);
    world.draw(context);
  },UPDATE_SPEED_MILISECONDS);

  canvasArea.addEventListener("click", function(e){
    world.addObj(
      Circle(getCursorPosition(e,canvasArea),
      Point2D(getRandomBetween(-MAX_RANDOM_VELOCITY,MAX_RANDOM_VELOCITY),
         getRandomBetween(-MAX_RANDOM_VELOCITY,MAX_RANDOM_VELOCITY)),
      CIRCLE_COLOR,CIRCLE_RADIUS,CIRCLE_ELASTICITY)
    );
  }, false);
}

function Point2D(x,y){
  return {
    x: x,
    y: y,

    sum: function(vec){
      return Point2D(this.x+vec.x, this.y+vec.y);
    },

    mul: function(scaler){
      return Point2D(this.x*scaler, this.y*scaler);
    }
  };
}

function Circle(coords,vel,color,radius,elasticity){
  return {
    coords: coords,

    vel: vel,

    color: color,

    radius: radius,

    elasticity: elasticity,

    update: function(acel,time){
      this.coords = this.coords.sum(this.vel.mul(time));
      this.vel = this.vel.sum(acel.mul(time));
    },

    handleColisions: function(topLeftPoint,bottonRightPoint){
      if(this.coords.x < topLeftPoint.x+radius){
        this.coords.x = topLeftPoint.x+radius;
        this.vel.x *= -1*this.elasticity;
      }
      if(this.coords.x > bottonRightPoint.x-radius){
        this.coords.x = bottonRightPoint.x-radius;
        this.vel.x *= -1*this.elasticity;
      }

      if(this.coords.y < topLeftPoint.y+radius){
        this.coords.y = topLeftPoint.y+radius;
        this.vel.y *= -1*this.elasticity;
      }
      if(this.coords.y > bottonRightPoint.y-radius){
        this.coords.y = bottonRightPoint.y-radius;
        this.vel.y *= -1*this.elasticity;
      }
    },

    draw: function (context){
      context.beginPath();
      context.arc(this.coords.x,this.coords.y,radius,0,Math.PI*2,true);
      context.fillStyle = color;
      context.fill();
      context.closePath();
    }
  };
}

function World(acel){
  return {
    topLeftPoint: null,
    bottonRightPoint: null,
    acel: acel,
    objs: [],

    setExtremePoints: function(topLeftPoint,bottonRightPoint){
      this.topLeftPoint = topLeftPoint;
      this.bottonRightPoint = bottonRightPoint;
    },

    addObj: function(obj){
      this.objs.push(obj);
    },

    update: function(time){
      for(index in this.objs){
        this.objs[index].update(acel,time);
        if(this.topLeftPoint != null && this.bottonRightPoint != null){
          this.objs[index].handleColisions(this.topLeftPoint,this.bottonRightPoint);
        }
      }
    },

    draw: function(context){
      context.clearRect(0,0,window.innerWidth, window.innerHeight);
      for(index in this.objs){
        this.objs[index].draw(context);
      }
    }
  };
}





function getCursorPosition(e, canvas) {
  var x;
  var y;
  if (e.pageX != undefined && e.pageY != undefined) {
    x = e.pageX;
    y = e.pageY;
  }
  else {
    x = e.clientX + document.body.scrollLeft +
    document.documentElement.scrollLeft;
    y = e.clientY + document.body.scrollTop +
    document.documentElement.scrollTop;
  }
  x -= canvas.offsetLeft;
  y -= canvas.offsetTop;


  return Point2D(x,y);
}

function getRandomBetween(min, max) {
  return Math.random() * (max - min) + min;
}